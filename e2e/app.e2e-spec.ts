import { AllInOneAngular2Page } from './app.po';

describe('all-in-one-angular2 App', function() {
  let page: AllInOneAngular2Page;

  beforeEach(() => {
    page = new AllInOneAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
