import { Album } from './album';
import { Image } from './image';

export interface Artist {
    id: string;
    name: string;
    genres: string[];
    albums: Album[];
    images: Image[];
}
