export interface Track {
    hduration_ms: number;
    explicit: boolean;
    href: string;
    id: string;
    name: string;
    track_number: number;
}
