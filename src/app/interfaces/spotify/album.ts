import { Image } from './image';
import { Track } from './track';

export interface Album {
    id: string;
    images: Image[];
    name: string;
    url: string;
    label: string;
    popularity: number;
    release_date: string;
    tracks: Track[];
}
