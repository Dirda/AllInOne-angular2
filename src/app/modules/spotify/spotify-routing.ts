import { RouterModule, Routes } from '@angular/router';

import { SpotifyComponent } from './spotify.component';
import { ArtistViewComponent } from './artist-view/artist-view.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
    {
        path: '',
        component: SpotifyComponent,
        children: [
          {
              path: '',
              component: SearchComponent
          },
          {
              path: 'artist/:id',
              component: ArtistViewComponent
          }
        ]
    }
];

export const spotifyRouting = RouterModule.forChild(routes);
