import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../../services/spotify/spotify.service';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Artist } from '../../../interfaces/spotify/artist';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchRes: Artist[];
  searchForm: FormGroup;

  constructor (
    private _spotifyService: SpotifyService,
    private formBuilder: FormBuilder,
    private _router: Router
  ) { }

  ngOnInit() {
      this.searchForm = setFormValues(this.formBuilder);
      this.searchForm.get('searchParam').setValue('Korn');
      this.searchMusic();
  }

  searchMusic() {
      const searchVal: string = this.searchForm.get('searchParam').value;
      if (searchVal) {
          this._spotifyService.searchMusic(searchVal)
              .subscribe(
                res => {
                    this.searchRes = res.artists.items;
                },
                err => {
                    console.log('invalid response');
                    console.log(err);
                }
              );
      } else {
          this.searchRes = null;
      }
  }

  openView(view: Artist) {
      this._router.navigate([this._router.url + '/artist', view.id]);
  }

}

function setFormValues(formBuilder: FormBuilder) {
    return formBuilder.group({
      'searchParam': [''],
    });
};
