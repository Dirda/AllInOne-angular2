import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SpotifyComponent } from './spotify.component';
import { ArtistViewComponent } from './artist-view/artist-view.component';
import { SearchComponent } from './search/search.component';

import { SpotifyService } from '../../services/spotify/spotify.service';
import { spotifyRouting } from './spotify-routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    spotifyRouting,
    NgbModule
  ],
  declarations: [
    SpotifyComponent,
    ArtistViewComponent,
    SearchComponent,
  ],
  providers: [
    SpotifyService
  ]
})
export class SpotifyModule { }
