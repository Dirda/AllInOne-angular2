import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../../services/spotify/spotify.service';
import { Artist } from '../../../interfaces/spotify/artist';
import { Album } from '../../../interfaces/spotify/album';

@Component({
  selector: 'app-artist-view',
  templateUrl: './artist-view.component.html',
  styleUrls: ['./artist-view.component.css']
})
export class ArtistViewComponent implements OnInit {
    artistId: string;
    artist: Artist;
    albums: Album[];
    album: Album;

  constructor(
    private _spotifyService: SpotifyService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
      console.log('View:');
      console.log(this._activatedRoute.snapshot.params['id']);
      this.routerOnActivate();
  }

  routerOnActivate(): void {
        this.artistId = this._activatedRoute.snapshot.params['id'];

        this.getArtist();
        this.getAlbums();
  }

  getArtist() {
      this._spotifyService.getArtist(this.artistId)
          .subscribe(
              res => {
                  this.artist = res;
                  console.log(this.artist);
              },
              err => {
                  console.log('invalid response');
                  console.log(err);
              }
          );
  }

  getAlbums() {
      this._spotifyService.getAlbums(this.artistId)
          .subscribe(
              res => {
                  this.albums = res.items;
              },
              err => {
                  console.log('invalid response');
                  console.log(err);
              }
          );
  }

  getAlbum(album: Album) {
      this._spotifyService.getAlbum(album.id)
          .subscribe(
              res => {
                  this.album = res;
                  this.album.tracks = res.tracks.items;
                  console.log('Album info:');
                  console.log(this.album);
                  console.log('Track info');
                  console.log(this.album.tracks);
                  console.log('++++++++++++++++++');
              },
              err => {
                  console.log('invalid response');
                  console.log(err);
              }
          );
  }

  getTrackTime(durationMs: number) {
      return this._spotifyService.getDuration(durationMs).toFixed(2).replace('.', ':') + ' min';
  }

}
